
$('html').removeClass('jsDisabled').addClass('jsEnabled').addClass('jsLoading');


$(function() {






$("header .mobileBlock").click(function() {
	$(this).parents("body").toggleClass("active-menu");
});


	//Главный слайдер
$('.slideListBlock').each(function(){
	var owner = this;

	var thresholds = [1920,1600,1440,1336,1280,1024,960,800,720,640,320,0];

	$('.image',this).each(function(){
		var owner = this;
		var img = new Image;
		img.onload = function(){
			for(i in thresholds)
			{
				if (img.width>=thresholds[i])
				{
					owner.setAttribute('data-width-threshold',thresholds[i]);
					break;
				}
			}
		};
		img.src = this.src;
	})

	var html = [];
	$('.slideList .slide',this).each(function(){
		html.push("<a href='#' class='slide'></a>");
	})

	$(this).append($("<div class='navBlock navBlock-steps'><div class='blockWrapper'><a href='#' class='step step-prev'>Предыдущий</a><a href='#' class='step step-next'>Следующий</a></div></div><div class='navBlock navBlock-slides'><div class='blockWrapper'>"+html.join('')+'</div></div>'));

	this.__switch = function(idx){
		if (typeof(idx)=='undefined')
		{
			this.__current++;
		}
		else
		{
			if (idx=='prev'||idx=='next')
			{
				this.__current += idx=='prev'?-1:1;
			}
			else
			{
				this.__current=idx;
			}
		}

		if (this.__current <0)
		{
			this.__current = $('.slideList .slide',owner).length;
		}
		if (this.__current >= $('.slideList .slide',owner).length)
		{
			this.__current=0;
		}

		$('.slideList .slide',owner).removeClass('slide-active').eq(this.__current).addClass('slide-active');
		$('.navBlock-slides .slide',owner).removeClass('slide-active').eq(this.__current).addClass('slide-active');
	}
	this.__switch(0);

	$(this).mouseleave(function(){
		this.__interval = window.setInterval(function(){
			owner.__switch();
		},($(owner).data('slide-interval') || 5)*1000);
	}).mouseenter(function(){
		if (this.__interval) {window.clearInterval(this.__interval);}
	}).mousemove(function(){
		if (this.__interval) {window.clearInterval(this.__interval);}
	})

	$('.navBlock-steps',this).on('click','.step',function(e){
		e.preventDefault();
		owner.__switch($(this).hasClass('step-prev') ? 'prev' : 'next');
		return false;
	});

	$('.navBlock-slides',this).on('click','.slide',function(e){
		e.preventDefault();
		owner.__switch($(this).index());
		return false;
	});
});
















	//галерея

  $('.imageGalleryBlock').each(function(){
		var owner = this;
		$(this).prepend($("<div class='previewBlock'><div class='imageBlock'><div class='imageWrapper'><img src='' class='image' /></div></div><div class='navBlock navBlock-steps'><a href='#' class='step step-prev'>Назад</a><a href='#' class='step step-next'>Вперёд</a></div></div>"));
		
		var $list = $('.imageList',this);
		var $outer = $list.parent();
    var $preview = $('.previewBlock',owner);
    
		$('.item:first-child',$list).addClass('item-active');
		
			
		$(this).on('click','.previewBlock',function(){
			var items = [];
			$('.imageList .item a',owner).each(function(){
				items.push({src:this.href,type:($(this).data('video')?'iframe':'image')});
			});
			var options = {items:items};
			$.magnificPopup.open($.extend(true,{},__opts.popup.gallery,options),$('.imageList .item-active').index());
			return false;
		});
		
		$(this).on('click','.navBlock .step',function(){
			var idx = $('.item-active',$list).index();
			idx += $(this).hasClass('step-prev')?-1:1;
			
			if (idx<0)
			{
				idx=$('.item',$list).length-1;
			}
			if (idx>$('.item',$list).length-1)
			{
				idx=0;
			}
			
			$('.item:eq('+idx+') a',$list).click();
			
			return false;	
		})
		
		$list.on('click','.item a',function(){
			var url=this.href;
      
      $('.imageBlock',$preview).fadeOut(500,function(){				
        var img = new Image();
        img.onload = function(){
          $('.previewBlock .image',owner).attr('src',this.src);
          $('.previewBlock .imageBlock',owner).fadeIn(500);
          $('.previewBlock',owner).removeClass('previewBlock-loading').addClass('previewBlock-image');
        }
        img.src=url;
      });
      


			$('.item',$list).removeClass('item-active');
			$(this.parentNode).addClass('item-active');
			return false;
		})
		
		$('.item:first-child a',$list).click();
	})












	$('html').find('input[type="date"]').each(function(){
		console.log("pick")
		this.type='text';
		$(this).addClass('isIndicated').parent().append($("<span class='indicator indicator-date' placeholder='"+this.getAttribute('placeholder')+"'></span>"));
		pickmeup(this,{
			locale: 'ru',
			hide_on_select: true,
			format: 'Y-m-d',
			default_date:false
		});
		
		this.addEventListener('pickmeup-change', function (e) {
			var val = e.detail.formatted_date;
			if (val)
			{
				val = val.split('-')
				val = val[2]+' '+pickmeup.defaults.locales['ru'].monthsShort[val[1]-1]+' '+val[0];
			}
			else
			{
				val = '';
			}
			$('+.indicator',this).html(val);
		})
		
		var val = this.value;
		if (val)
		{
			val = val.split('-')
			val = val[2]+' '+pickmeup.defaults.locales['ru'].monthsShort[val[1]-1]+' '+val[0];
		}
		$('+.indicator',this).html(val);
	})


  




  $('html').removeClass('jsLoading').addClass('jsLoaded');
});